// Localstrategy, needed for passport.
var LocalStrategy = require('passport-local');

// required for the database connection.
var mongojs = require('mongojs');

// required for the name of the database.
var config = require('./config');

// connect to the database
var userLogic = require('./Logic/UserLogic');

// used to check the passwords
var bcrypt = require('bcryptjs');


// The strategy to be used.
var strategy = new LocalStrategy(
    // Function which checks whether the user credentials matches someone in the database.
    function (username, password, done) {
        //console.log(username);
        // Search for the user based on the username
        userLogic.getUser({
            username: username
        }, function (err, user) {
            // in case of error, return the error.
            if (err) {
                return done(err);
            }
            // If there is no user with the given credentials, he cannot log in.
            if (!user) {
                return done(null, false, {
                    message: "Incorrect username or password!"
                });
            }
            // If the users password does not matches the one in the database, 
            // he gave the wrong emailadres.
            if (bcrypt.compareSync(user.password, password)) {
                return done(null, false, {
                    message: "Incorrect username or password"
                });
            }
            // If everything succeeds and the user credentials is not correct.
            return done(null, user);
        });
    }
);

/*
 * Function needed by passport to serialize the user into a session
 */
var serializeUser = function (user, done) {
    done(null, user._id);
}

/*
 * Deserializes a user by finding the user corresponding to the id stored
 * in session.
 */
var deserializeUser = function (id, done) {
    userLogic.getUser({
        _id: mongojs.ObjectId(id)
    }, function (err, user) {
        done(err, user);
    });
}


// export the strategy to be used.
module.exports = {
    strategy: strategy,
    serializeUser: serializeUser,
    deserializeUser: deserializeUser
}