(function(App){
    App.View.NumberParamView = Backbone.Marionette.ItemView.extend({
        template: "#numberparam-tpl",
        className: 'numberparam-container',
        tagName: "div",
    });
})(window.App);