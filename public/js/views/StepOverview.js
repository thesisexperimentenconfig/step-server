(function (App) {
    
    App.View.StepOverview = Backbone.Marionette.CompositeView.extend({
        
        tagName: 'div',
        id: "step-overview",
        childView: App.View.StepView,
        template: "#stepoverview-tpl",
        
        initialize: function(options){
            this.collection = options.collection;
        }
        
    

    });
})(window.App);