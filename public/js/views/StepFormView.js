(function(App){

    App.View.StepFormView = Backbone.Marionette.ItemView.extend({
        template: "#stepform-tpl",
        id:"stepformview",
        
        events:{
            'change #type': 'showTypeForm',
            'click #savestep': 'saveStep',
            'click #addparam': 'addParameter',
            'click #addoption': 'addOption'
        },
        
        initialize: function(){
            this.currentParamType = 'none';
            this.parameters = {};
            this.options = [];
        },
        
        
        showTypeForm: function(event){
            var selected = $(event.target).val();
            $("#param-config").empty();
            
            this.options = [];
            
            if(selected === 'number'){
                
                var view = new App.View.NumberParamView();
                view.render();
                $("#param-config").append(view.el);
            }
            
            if(selected === 'enum'){
                var view = new App.View.EnumParamView();
                view.render();
                $("#param-config").append(view.el);
            }
            
            this.currentParamType = selected;
        },
        
        saveStep: function(){
            var step = {};
            if($('#name').val() === ''){
                return alert('Name for the step is required!')
            }
            
            if($('#input').val() === ''){
                return alert('Input for the step is required')
            }
            
            if($('#output').val() === ''){
                return alert('output for the step is required')
            }
            
            step.name = $('#name').val();
            step.description = $('#description').val();
            step.parameters = this.parameters;
            var output = $('#output').val();
            var input = $('#input').val()
             step.output = output.split(",");
             step.input = input.split(",");
            
            var model = new App.Model.StepModel(step);
            console.log(step);
            model.save();
            
            window.location.hash = '';
        },
        
        showParameter: function(parameter){
            var self = this;
            $('#parameters').show();
            
            var htmlrep = '<div class="parameter" id="param' + parameter.name + '">';
            
            for(var key in parameter){
                htmlrep += '<b>' + key + '</b>: ' + parameter[key] + ', ';
            }
            
            htmlrep += '<div class="deleteParam"  id="delete' 
                        + parameter.name +'" ><i class="fa fa-times" data-id="' + parameter.name +'"></i></div></div>';
         
            
            $('#parameters').append(htmlrep);
            
            $('#delete' + parameter.name).on('click', function(event){
                event.stopPropagation();
                var name = $(event.target).data('id');
                $('#param' + name).remove();
                
                delete self.parameters[name];
            });
            
        },
        
        addParameter: function(){
            var parameter = {};
            
            if($('#paramName').val() === ''){
                return alert('parameter name required');
            }
            
            
            
            if($('#value').val() === ''){
                return alert('Default value required');
            }
            
            if(this.currentParamType === 'number'){
                if($('#min').val() === '' || !$.isNumeric($('#min').val())){
                    return alert('Min value should be a number')
                }
                
                if($('#max').val() === '' || !$.isNumeric($('#max').val())){
                    return alert('Max value should be a number')
                }
                
                if($('#max').val() <= $('#min').val()){
                    return alert('Max should be greater or equal to min')
                }
                
                if(!$.isNumeric($('#value').val())){
                    return alert('Default Value should be numeric');
                }
                if(parseFloat($('#value').val()) <= parseFloat($('#min').val())){
                    return alert('default value should be greater than min value');
                }
                
                if(parseFloat($('#value').val()) >= parseFloat($('#max').val())){
                    return alert('default value should be less than max value');
                }
                parameter.type = 'number';
                parameter.min = $('#min').val();
                parameter.max = $('#max').val();
                parameter.unit = $('#unit').val();
            }
            
            if(this.currentParamType === 'enum'){
                if(this.options.length === 0){
                    return alert('Cannot add an enumeration without options!');
                }
                
                parameter.values = this.options.slice(0);
            }
            
            parameter.required = $('#required').is(':checked');
            parameter.name = $('#paramName').val();
            parameter.default = $('#value').val();
            
            $('#optionList').empty();
            $('#optionList').hide();
            
            
            this.options = [];
            this.parameters[parameter.name] = parameter;
            this.showParameter(parameter);         
        },
        
        addOption: function(){
            
            if($('#option').val() === ''){
                return alert('cannot add empty option');
            }
            
            var option = $('#option').val();
            this.options.push(option);
            $('#optionList').show();
            $('#optionList').append('<div class="opt">' + option + '</div>');
            $('#option').val('');
            alert('option added');
            
        }
        
    })
})(window.App);
