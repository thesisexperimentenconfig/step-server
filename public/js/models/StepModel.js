(function(App){
    App.Model.StepModel = Backbone.Model.extend({
        urlRoot:'/steps',
        idAttribute: "_id"
    })
})(window.App);