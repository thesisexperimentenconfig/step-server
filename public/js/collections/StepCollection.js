(function(App){
    App.Collection.StepCollection = Backbone.Collection.extend({
        model: App.Model.StepModel,
        url: '/steps'
    })
})(window.App)