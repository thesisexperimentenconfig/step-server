(function (App) {
    App.Rtr = Marionette.AppRouter.extend({
        
        appRoutes: {
            "remove/:id": "deleteStep",
            "addstep": "addStep",
            "": "home"
        }
    });
})(window.App);