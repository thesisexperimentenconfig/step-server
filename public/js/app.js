var App = new Backbone.Marionette.Application();

window.App = App;

_.extend(App, {
    View: {},
    Model: {},
    Collection: {}
});

App.addRegions({
    main: '#main'
});

var initTemplates = function () {
    // Load in external templates
    var ts = [];
    _.each(document.querySelectorAll('[type="text/x-template"]'), function (el) {
        var d = Q.defer();
        $.get(el.src, function (res) {
            el.innerHTML = res;
            d.resolve(true);
        });
        ts.push(d.promise);
    });

    return Q.all(ts);
};

var getSteps = function () {
    d = Q.defer();
    var collection = new App.Collection.StepCollection();
    collection.fetch({
        success: function () {
            d.resolve(collection);
        }
    });
    return d.promise;

}

App.addInitializer(function () {
    initTemplates()
        .then(getSteps)
        .then(function (stepCollection) {
            console.log(stepCollection.models);
            var controller = new App.Cntrller({
                collection: stepCollection
            });
            var router = new App.Rtr({
                controller: controller
            });

            Backbone.history.start();
        });

});