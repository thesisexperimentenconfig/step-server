(function (App) {
    App.Cntrller = Backbone.Marionette.Controller.extend({

        initialize: function (options) {
            this.collection = options.collection;
        },

        home: function () {
            var self = this;
            var b = new App.View.StepOverview({
                collection: self.collection
            });
            b.render();
            App.main.show(b);
        },
        deleteStep: function (id) {
            if(confirm("Are you sure you want to delete this model?")){
                var model = this.collection.get(id);
                model.destroy();
            };
        },

        addStep: function () {
            var self = this;
            this.animateOut().then(function(){
                var view = new App.View.StepFormView();
                App.main.show(view);
                self.animateIn();
            })
        },

        animateOut: function () {
            var d = Q.defer();
            $("#main").fadeOut(200,function(){
                d.resolve();
            });
            
            return d.promise;
        },
        
        animateIn: function(){
            $("#main").fadeIn(200,function(){
                d.resolve();
            });
        }


    });
})(window.App);