<div class="device">
    <div class="title"><%= name %></div>
    <div class="params">
       <% var hasParameters = false; var nb = 0%>
        <% for(key in parameters){  %>
           <% hasParameters = true; nb++ %>
            <div class="parameter">
                <%= parameters[key].name %>
            </div>
            <% if(nb == 3){%>
                ...
            <% break;} %>
        <% } %>
        
        <% if(!hasParameters){ %>
       <div class="parameter" style="color: #6f5050; font-weight: bold">
                No parameters
      </div>
        
    <% }%>
    
    </div>
    
    <div class="options">
        <a href="#remove/<%= _id %>"><i class="fa fa-trash"></i></a>
    </div>

</div>
