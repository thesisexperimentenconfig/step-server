<h1>Create a new Step</h1>
<label for="name">Step Name* </label>
<input type="text" name="name" id="name" placeholder="Step name">
<label for="name">Step Input* </label>
<input type="text" name="input" id="input" placeholder="Step Input">
<label for="name">Step Output* </label>
<input type="text" name="output" id="output" placeholder="Step Output">
<label for="description">Description</label>
<textarea name="description" id="description" rows="10" placeholder="Step Description"></textarea>
<label for="parameters">Add Parameters</label>
<select name="type" id="type">
    <option disabled selected> -- select an option -- </option>
    <option value="number">Number</option>
    <option value="enum">Enumeration</option>
</select>

<div id="param-config">
    
</div>
<div id="parameters" style="display:none">

<div class="title"> Parameters </div>
</div>

<button id="savestep">Save Step</div>