var express = require('express');

// Required to create a router
var router = express.Router();

// required for the name of the database
var config = require('../config');

// User logic is seperated for reuse
var modelLogic = require("../logic/StepLogic");


router.get('/', function(req,res){
    var steps = modelLogic.getSteps(function(err, steps){
        if(err) console.log(err);
        else res.send(steps);
    });
})

router.post('/', function(req,res){
    if(!req.isAuthenticated()){
        return res.sendStatus(403);
    }
    
    var model = req.body;
    modelLogic.addStep(model);
    res.send(true);
})

router.delete('/:stepId', function(req,res){
    
    if(!req.isAuthenticated()){
        return res.sendStatus(403);
    }
    
    var id = req.params.stepId;
    modelLogic.deleteStep(id);
     res.send(true);
});

router.put('/:stepId', function(req,res){
    if(!req.isAuthenticated()){
        return res.sendStatus(403);
    }
    
    var model = req.body;
    modelLogic.updateStep(model);
    res.send(true);
});

module.exports = router;