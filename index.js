/*
 * DEPENDENCIES
 * ============
 */

// Express is used to handle http requests
var express = require('express');

// path is required to setup the apps views and public folder
var path = require('path');

// filesystem access.
var fs = require('fs');

// Compress for gzipped traffic between server and client
var compress = require('compression');

// used to the fields of a request
var bodyParser = require('body-parser');

// express-session is required for session handling
var session = require('express-session');

// use passport for authentication
var passport = require('passport');

// userStrategy to login
var userStrategy = require('./userStrategy');

// contains the configuration of the server
var config = require('./config');


/*
 * Application configuration
 * =========================
 */

var app = express();
// Set the views
app.set('views', path.join(__dirname, 'views'));

// Set a render engine to show the views
app.set('view engine', 'ejs');

// make the public folder statically accessible
app.use(express.static(path.join(__dirname, 'public')));

// tell the app to use compression with requests and results
app.use(compress());

// tell the application to use the parser to interpret the request.
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: false
}));
// user strategy for passport
passport.use(userStrategy.strategy);


// needed for serialisation en deserialization of user in session
passport.serializeUser(userStrategy.serializeUser);
passport.deserializeUser(userStrategy.deserializeUser);

app.use(session({secret: 'mySecretKey'}));

// Passport must be initialized
app.use(passport.initialize());

// needed for persistent session!
app.use(passport.session());





/*
 * Configure routes
 *
 */

var steps = require('./routes/StepRouter');
app.use('/steps', steps);


app.get('/', function (req, res) {
    res.render("login");
});

app.get('/admin', function(req, res){
    if(!req.isAuthenticated()){
        return res.redirect('/');
    }

    
    res.render("index");
})


app.post('/login', passport.authenticate('local'), function(req,res){
    res.redirect("/admin");
});

/*
 * Server Start
 * ============
 */

// the port on which the server listens, can be changed in the configuration file.
var port = config.port;

// Listen to incoming requests
var server = app.listen(port, function () {
    console.log("App started on port " + port);
});