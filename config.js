module.exports = {
    // The port on which the application runs
    port: 7773,
    // If the database is not local, replace with the url!
    database: "inspector"
}