// Set up the router
var express = require('express');

// required for the database connection
var mongojs = require('mongojs');

// required for the name of the database
var config = require('../config');

// make a connection with the database.
var db = mongojs(config.database, ['users']);

// required to encrypt the password
var bcrypt = require('bcryptjs');

// fields required for the user.
var requiredFields = ['username', 'password'];


/*
 * get all the users.
 */
getUsers = function (callback) {
    db.users.find(function (err, docs) {
        callback(err, docs);
    });
}



/*
 * Adds a new user to the database.
 * First checks whether the username is already in the database.
 * If present, the user is not added.
 *
 * a status report is returned.
 */
var addUser = function (user, callback) {

    // check if all required fields are present, are matching
    if (!checkRequiredFields(user)) {
        callback(null, {
            success: false,
            message: "Not all required fields are filled in"
        });
        return;
    }

    // encrypt password
    user.password = bcrypt.hashSync(user.password);

    // check whether there is a user present with the same username
    db.users.findOne({
        username: user.username
    }, function (err, doc) {
        if (!err) {
            // insert the user if there is no conflict
            if (!doc) {
                db.users.insert(user);
                callback(err, {
                    success: true,
                    message: "User created!"
                });
            } else {
                callback(err, {
                    success: false,
                    message: "The username is not available!"
                });
            }
        }
    })

}

/*
 * Iterates over the required fields,
 * if no required fields are missing, returns true, false otherwise.
 */
var checkRequiredFields = function (user) {

    for (var i = 0; i < requiredFields.length; i++) {
        var field = requiredFields[i];
        if (!user[field] || user[field] == '') {
            return false;
        }
    }

    return true;

}

/*
 * find one user based on the query. Result is passed
 * to the callback function
 */
var getUser = function (query, callback) {
    db.users.findOne(query, function (err, doc) {
        if (err) console.log(err);
        if (doc) {
            callback(null, doc);
        } else {
            callback(new Error("No user found"), null);
        }
    })
}


module.exports = {
    getUsers: getUsers,
    addUser: addUser,
    getUser: getUser
}