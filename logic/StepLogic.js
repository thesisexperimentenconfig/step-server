// database access
var mongojs = require('mongojs');

// configuration file
var config = require('../config');

// create connection with database
var db = mongojs(config.database, ['steps']);

/**
 * Add a Step to the database.
 */
var addStep = function(model){
    db.steps.insert(model);
}

/**
 * update a step in the database
 */
var updateStep = function(model, callback){
    var id = mongojs.ObjectId(model._id)
    db.steps.update({
        _id: id 
    }, model);
}

/**
 * Delete a step from the database
 */
var deleteStep = function(stepId){
    var id = mongojs.ObjectId(stepId);
    db.steps.remove({
        _id: id
    });
}

/**
 * Return the steps from the database
 */
var getSteps = function(callback){
    db.steps.find(function(err, models){
        callback(err, models);
    });
}

module.exports = {
    addStep: addStep,
    updateStep: updateStep,
    deleteStep: deleteStep,
    getSteps: getSteps
}