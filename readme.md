# How to use

- Make sure you have Node.js installed.
- Make sure you have MongoDB installed.
- install this app with `npm install`
- initialize the database by running `node initSteps.js` when the word done is printed, you can quit.
- Change the username and password in `insertUser.js` to the one you desire (password is stored with bcrypt)
- Insert the user with `node insertUser.js` when "Created new user!" is printed, you can quit.
- Run the webserver with `node index.js`
- Navigate to `http://localhost:7773/`
